module chainmaker.org/chainmaker/chainconf/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.0
	chainmaker.org/chainmaker/logger/v2 v2.3.0
	chainmaker.org/chainmaker/pb-go/v2 v2.3.0
	chainmaker.org/chainmaker/protocol/v2 v2.3.0
	chainmaker.org/chainmaker/utils/v2 v2.3.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
	github.com/golang/mock v1.6.0
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
)
